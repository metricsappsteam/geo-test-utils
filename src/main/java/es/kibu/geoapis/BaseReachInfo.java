/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis;

import es.kibu.geoapis.common.es.kibu.geoapis.common.gpx.Utils;
import org.joda.time.DateTime;
import org.joda.time.Interval;

/**
 * Created by lrodr_000 on 14/03/2016.
 */
public class BaseReachInfo implements ReachInfo {

    private int pointsUnderReach = 0;
    private DateTime beginReach;
    private DateTime endReach;
    private Double minDistance = null;
    private Double maxDistance = null;
    private Location onceReached;
    private double radius;

    public BaseReachInfo(){

    }

    public void updateReachInfo(Location.DataPoint poi, Double distance, boolean updateCount) {

        if (distance!= null){
            maxDistance = Utils.updateMax(maxDistance, distance);
            minDistance = Utils.updateMin(minDistance, distance);
        }

        if (pointsUnderReach == 0) {
            beginReach = poi.getDate();
        }

        endReach = Utils.updateMax(endReach, poi.getDate());
        if (updateCount) {
            pointsUnderReach++;
        }

    }

    public void updateReachInfo(Location.DataPoint poi, Double distance) {
        updateReachInfo(poi, distance, true);
    }

    /*public BaseReachInfo (DateTime beginReach, DateTime endReach){
        this.beginReach = beginReach;
        this.endReach = endReach;
    }*/

    public BaseReachInfo(double radius, Location onceReached) {
        this.radius = radius;
        this.onceReached = onceReached;
    }

    @Override
    public Interval getTimeRange() {
        if (getBeginTime() != null && getEndTime() != null) {
            return new Interval(getBeginTime(), getEndTime());
        }
        return null;
    }

    @Override
    public Double getMaxDistance() {
        return maxDistance;
    }

    @Override
    public Double getMinDistance() {
        return minDistance;
    }

    @Override
    public Integer getPointsUnderReach() {
        return pointsUnderReach;
    }

    @Override
    public DateTime getBeginTime() {
        return beginReach;
    }

    @Override
    public DateTime getEndTime() {
        return endReach;
    }

    @Override
    public Location getReachedLocation() {
        return onceReached;
    }

    @Override
    public double getRadius() {
        return radius;
    }
}
