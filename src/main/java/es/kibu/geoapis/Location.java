/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.format.ISODateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by lrodr_000 on 22/02/2016.
 */
public class Location implements Serializable {

    List<DataPoint> points = new ArrayList<>();

    public boolean addPoint(DataPoint point){
        return points.add(point);
    }

    public static class LatLong {

        double latitude;
        double longitude;

        public double getLongitude() {
            return longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }
        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof LatLong)) return false;

            LatLong latLong = (LatLong) o;

            if (Double.compare(latLong.getLatitude(), getLatitude()) != 0) return false;
            return Double.compare(latLong.getLongitude(), getLongitude()) == 0;
        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            temp = Double.doubleToLongBits(getLatitude());
            result = (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(getLongitude());
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }

        @Override
        public String toString() {
            return "LatLong{" +
                    "latitude=" + latitude +
                    ", longitude=" + longitude +
                    '}';
        }
    }

    //TODO: this should be enhanced to be able to use other types of locations.
    public static class DataPoint {

        private DateTime date;

        public LatLong getLocation() {
            return location;
        }

        public void setLocation(LatLong location) {
            this.location = location;
        }

        public DateTime getDate() {
            return date;
        }

        public void setDate(DateTime date) {
            this.date = date;
        }

        private LatLong location;

       /* static DataPoint from(LocationUpdateParams.DataPoint dp) {
            DataPoint dataPoint = new DataPoint();
            dataPoint.date = dp.date.date;
            dataPoint.location = new LatLong();
            dataPoint.location.setLatitude(dp.location.position.latitude);
            dataPoint.location.setLongitude(dp.location.position.longitude);
            return dataPoint;
        }
*/
       /* *//**
         * Note that the returned datapoint does not contain the client data!
         *//*
        public LocationUpdateParams.DataPoint toDataPoint(){
            DataPoint dataPoint = this;
            LocationUpdateParams.DataPoint dp = new LocationUpdateParams.DataPoint();
            dp.date = new BaseParams.ISO8601Date(dataPoint.date);
            dp.location = new LocationUpdateParams.PointLocation();
            dp.location.position = new LocationUpdateParams.Position();
            dp.location.position.latitude = dataPoint.location.getLatitude();
            dp.location.position.longitude = dataPoint.location.getLongitude();
            return dp;
        }*/

        @Override
        public String toString() {
            String dateString = ISODateTimeFormat.dateTime().print(date);
            return "DataPoint{" +
                    "date=" + dateString +
                    ", location=" + location +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof DataPoint)) return false;

            DataPoint dataPoint = (DataPoint) o;

            if (getDate() != null ? !getDate().equals(dataPoint.getDate()) : dataPoint.getDate() != null) return false;
            return getLocation() != null ? getLocation().equals(dataPoint.getLocation()) : dataPoint.getLocation() == null;

        }

        @Override
        public int hashCode() {
            int result = getDate() != null ? getDate().hashCode() : 0;
            result = 31 * result + (getLocation() != null ? getLocation().hashCode() : 0);
            return result;
        }
    }

    /*public static Location fromLocationParams(LocationUpdateParams params) {
        Location location = new Location();
        for (LocationUpdateParams.DataPoint dataPoint: params.getPoints()) {
            location.addPoint(DataPoint.from(dataPoint));
        }
        return location;
    }

    public LocationUpdateParams toLocationUpdateParams(){

        Location location = this;
        return toLocationUpdateParams(location);
    }

    public static LocationUpdateParams toLocationUpdateParams(Location location) {
        LocationUpdateParams params = new LocationUpdateParams();
        List<LocationUpdateParams.DataPoint> dataPoints = new ArrayList<>();
        for (DataPoint dataPoint : location.getPoints()) {
            LocationUpdateParams.DataPoint dp = dataPoint.toDataPoint();
            dataPoints.add(dp);
        }
        params.setPoints(dataPoints);
        return params;
    }*/

    public List<DataPoint> getPoints() {
        return points;
    }

    public List<DataPoint> getDataPointsSorted() {

        final DateTimeComparator instance = DateTimeComparator.getInstance();

        List<DataPoint> points = this.getPoints();

        //sort this by time as there as there is not garantee that it will be sorted
        points.sort(new Comparator<DataPoint>() {
            @Override
            public int compare(Location.DataPoint dt, Location.DataPoint dt1) {
                return instance.compare(dt.date, dt.date);
            }
        });

        return points;
    }

    public static boolean isSorted(List<DataPoint> points){
        final DateTimeComparator instance = DateTimeComparator.getInstance();
        boolean result = true;
        Comparator<DataPoint> comparator = new Comparator<DataPoint>() {
            @Override
            public int compare(Location.DataPoint dt, Location.DataPoint dt1) {
                return instance.compare(dt.date, dt.date);
            }
        };

        for (int i = 0; i < points.size()-1; i++) {
            int compare = comparator.compare(points.get(i), points.get(i + 1));
            result = result && (compare == 0 || compare == -1);
            if (!result) {
                return result;
            }
        }

        return result;
    }

    public DataPoint lastPoint(){
        List<DataPoint> points = getPoints();
        if (points.size() == 0) {
            return null;
        }
        return points.get(points.size()-1);
    }

    public DataPoint uniquePoint(){
        List<DataPoint> points = getPoints();
        if (points.size() > 1) throw new RuntimeException("Invalid assumption exception, there is not an unique point");
        return lastPoint();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;

        Location location = (Location) o;

        return getPoints() != null ? getPoints().equals(location.getPoints()) : location.getPoints() == null;

    }

    @Override
    public int hashCode() {
        return getPoints() != null ? getPoints().hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Location{" +
                "points=" + points +
                '}';
    }
}
