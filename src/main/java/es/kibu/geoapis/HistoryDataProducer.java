/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis;

import es.kibu.geoapis.common.es.kibu.geoapis.common.gpx.*;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;

public class HistoryDataProducer {

    protected DateTimeFormatter dateTimeFormatter = ISODateTimeFormat.dateTime();
    private Logger logger = LoggerFactory.getLogger(HistoryDataProducer.class);

    private List<LocationHistory.UntilReachHistory> locationsHistoryDwelling1000m;
    private List<LocationHistory.UntilReachHistory> locationsHistoryEntering1000m;
    private List<LocationHistory.UntilReachHistory> locationsHistoryExiting1000m;

    public HistoryDataProducer() {
    }

    public List<LocationHistory.UntilReachHistory> getLocationsHistoryBeforeATime(Gpx gpx, DateTime dateTime) {

        for (TrkType track : gpx.getTrk()) {
            Gpx.TrackInfo trackInfo = gpx.getTrackInfo(track);
            assertTrue(trackInfo.getInterval().toDuration().toStandardSeconds().size() > 0);


            logger.trace("/*-------------------------------------------------------------------------------------*/");
            logger.trace(String.format("Track: %s", track.getName()));
            logger.trace("/*-------------------------------------------------------------------------------------*/");
            LocationHistory history = historyFromTrack(track, dateTime);

        }

        return null;
    }

    public List<LocationHistory.UntilReachHistory> getLocationsHistoryExiting1000m() {
        return locationsHistoryExiting1000m;
    }

    public List<LocationHistory.UntilReachHistory> getLocationsHistoryEntering1000m() {
        return locationsHistoryEntering1000m;
    }

    public List<LocationHistory.UntilReachHistory> getLocationsHistoryDwelling1000m() {
        return locationsHistoryDwelling1000m;
    }

    public Location fromGPXWayPoint(WptType waypoint) {
        if (waypoint.getTime()!= null) {
            Location location = new Location();
            Location.DataPoint dataPoint = new Location.DataPoint();

            Location.LatLong latLong = new Location.LatLong();
            latLong.setLatitude(waypoint.getLat().doubleValue());
            latLong.setLongitude(waypoint.getLon().doubleValue());

            dataPoint.setLocation(latLong);
            dataPoint.setDate(fromTime(waypoint.getTime()));

            location.getPoints().add(dataPoint);
            return location;
        }
        else return null;

    }

    public LocationHistory historyFromTrack(TrkType track, int count) {
        return historyFromTrack(track, (history, location) -> history.size() < count);
    }

    public LocationHistory historyFromTrack(TrkType track, Interval interval) {
        return historyFromTrack(track, new LocationHistory.IntervalCondition(interval));
    }

    public LocationHistory historyFromTrack(TrkType track, DateTime untilTime) {
        return historyFromTrack(track, new LocationHistory.UntilDateTimeCondition(untilTime));
    }

    public LocationHistory.UntilReachHistory historyFromTrackWithInfo(TrkType track, Location onceReached, TracksParams params) {

        boolean exiting = params.exitTrack;

        LocationHistory.ReachInfoExtractor condition = (!exiting) ? new LocationHistory.ReachInfoExtractor(onceReached, params.radius)
                : new LocationHistory.ExitInfoExtractor(onceReached, params.radius);

        condition.setHaltOnEnter(params.haltOnEnter || exiting);

        LocationHistory locationHistory = historyFromTrack(track).trimUnOrdered();
        locationHistory = locationHistory.filter(condition);
        if (locationHistory == null) {
            return null;
        }

        return new LocationHistory.UntilReachHistory(locationHistory, condition);
    }

    public LocationHistory historyFromTrack(TrkType track) {
        return historyFromTrack(track, (LocationHistory.IterationCondition) null);
    }

    public LocationHistory historyFromTrack(TrkType track, LocationHistory.Condition condition) {
        LocationHistory history = new LocationHistory();
        int skipped = 0;

        for (TrksegType trksegType : track.getTrkseg()) {
            for (WptType wptType : trksegType.getTrkpt()) {
                Location location = fromGPXWayPoint(wptType);
                if (location!= null && (condition == null || condition.conditionApplies(history, location))) {
                    history.add(location);
                } else {
                    skipped++;
                }
            }
        }

        logger.trace(String.format("Element skipped: %d", skipped));

        return history;
    }

    DateTime fromTime(XMLGregorianCalendar time) {
        return Utils.fromTime(time);
    }

    public List<LocationHistory.UntilReachHistory> prepareForAllTracks(TracksParams params) throws Exception {
        List<LocationHistory.UntilReachHistory> fullList = new ArrayList<LocationHistory.UntilReachHistory>();

        for (SampleTracks.Sample sample : SampleTracks.getAllTracks()) {
            Gpx gpx1 = sample.getGpx();
            List<LocationHistory.UntilReachHistory> list = prepareTracks(gpx1, params);
            fullList.addAll(list);
        }
        return fullList;
    }

    public List<LocationHistory.UntilReachHistory> prepareTracks(Gpx gpx, TracksParams params) {

        List<LocationHistory.UntilReachHistory> preparedInfoList = new ArrayList<LocationHistory.UntilReachHistory>();

        for (WptType wptType : gpx.getWpt()) {

            Location onceReached = fromGPXWayPoint(wptType);
            if (onceReached == null) {
                logger.error("onceReached  is null ignoring");
            }
            else {
                for (TrkType trkType : gpx.getTrk()) {
                    Gpx.TrackInfo trackInfo = gpx.getTrackInfo(trkType);
                    assertTrue(trackInfo.getInterval().toDuration().toStandardSeconds().size() > 0);

                    logger.trace("/*-------------------------------------------------------------------------------------*/");
                    logger.trace(String.format("WayPoint: %s, Track: %s", wptType.getName(), trkType.getName()));
                    logger.trace("/*-------------------------------------------------------------------------------------*/");

                    LocationHistory.UntilReachHistory info = historyFromTrackWithInfo(trkType, onceReached, params);


                    if (info!= null && params.shouldInclude(info)) {
                        preparedInfoList.add(info);
                    }
                }
            }

        }
        return preparedInfoList;
    }

    boolean equalsReachInfo(ReachInfo info1, ReachInfo info2) {

        if (info1 == null || info2 == null) {
            return false;
        }

        if (info1.getPointsUnderReach() == info2.getPointsUnderReach() && info2.getPointsUnderReach() == 0) {
            return true;
        }

        boolean equalBeginReach = (info1.getBeginTime() == info2.getBeginTime()) || info1.getBeginTime().isEqual(info2.getBeginTime());

        boolean equalEndReach = (info1.getEndTime() == info2.getEndTime()) || info1.getEndTime().isEqual(info2.getEndTime());
        boolean equalMaxDistances = info1.getMaxDistance().equals(info2.getMaxDistance());
        boolean equalMinDistances = info1.getMinDistance().equals(info2.getMinDistance());
        boolean equalRadiuses = info1.getRadius() == info2.getRadius();

        return equalBeginReach &&
                equalEndReach &&
                equalMaxDistances &&
                equalMinDistances &&
                equalRadiuses;
    }

    protected void chekInfo(int radius, LocationHistory locationHistory, LocationHistory.UntilReachHistory info) {

        ReachInfo reachInfo = info.getInfo();

        if (reachInfo.getPointsUnderReach() > 0) {
            assertTrue(reachInfo.getMaxDistance() <= radius);
            assertTrue(reachInfo.getMinDistance() <= reachInfo.getMaxDistance());
        } else {
            logger.debug("No point was reached so skipping");
        }
        assertTrue(locationHistory.isOrdered(true));
        assertTrue(locationHistory.size() > 0);
    }

    protected void checkTrackTime(Gpx gpx, TrkType trkType) {

        Gpx.TrackInfo trackInfo = gpx.getTrackInfo(trkType);

        String begin = dateTimeFormatter.print(trackInfo.getInterval().getStart());
        String end = dateTimeFormatter.print(trackInfo.getInterval().getEnd());

        logger.debug(String.format("Interval : %s -- %s", begin, end));

        LocationHistory locationHistory = historyFromTrack(trkType, trackInfo.getInterval());
        assertTrue(locationHistory.size() == trackInfo.getTotalWayPoints());
    }



    public void checkTrack(Gpx gpx, String sampleName, TracksParams params) {

        logger.debug("/*-------------------------------------------------------------------------------------*/");
        logger.debug(String.format("Analysing a GPX (%s)", gpx.getMetadata() == null ? sampleName : sampleName + " - " + gpx.getMetadata().getName()));
        logger.debug("/*-------------------------------------------------------------------------------------*/");

        int wayPointIndex = 0;
        int radius = params.radius;

        for (WptType wptType : gpx.getWpt()) {
            Location onceReached = fromGPXWayPoint(wptType);
            if (onceReached == null) {
                logger.error("onceReached is null possibly because of problems in the time field {}.. ignoring", wptType.toString());
            }
            int trackIndex = 0;

            for (TrkType trkType : gpx.getTrk()) {
                logger.trace("/*-------------------------------------------------------------------------------------*/");
                logger.trace(String.format("WayPoint: %s (index: %d), Track: %s Index:%d", wptType.getName(), wayPointIndex, trkType.getName(),
                        trackIndex));
                logger.trace("/*-------------------------------------------------------------------------------------*/");

                LocationHistory.UntilReachHistory info = historyFromTrackWithInfo(trkType, onceReached, params);
                if (info!= null){
                    LocationHistory locationHistory = info.getHistory();
                    LocationHistory.UntilReachHistory info2 = locationHistory.filterUntilReachPoint(onceReached, radius);
                    LocationHistory locationHistory2 = info2.getHistory();

                /*if (locationHistory.size() != locationHistory2.size()) {
                    logger.debug("Different histories sizes will make this fail");
                }
                */
                    assertEquals(locationHistory.size(), locationHistory2.size());
                    assertTrue(equalsReachInfo(info.getInfo(), info2.getInfo()));

                    chekInfo(radius, locationHistory, info);
                }

                trackIndex++;
            }

            wayPointIndex++;
        }
    }

    public void checkStripper(Gpx gpx) {
        logger.debug("/*-------------------------------------------------------------------------------------*/");
        logger.debug(String.format("Analysing a GPX, with teh stripper functionality (%s)", gpx.getMetadata() == null ?
                "no name" : gpx.getMetadata().getName()));
        logger.debug("/*-------------------------------------------------------------------------------------*/");

        for (WptType wptType : gpx.getWpt()) {
            //Location onceReached = fromGPXWayPoint(wptType);
            for (TrkType trkType : gpx.getTrk()) {

                logger.trace("/*-------------------------------------------------------------------------------------*/");
                logger.trace(String.format("WayPoint: %s, Track: %s", wptType.getName(), trkType.getName()));
                logger.trace("/*-------------------------------------------------------------------------------------*/");

                LocationHistory locationHistory = historyFromTrack(trkType);
                LocationHistory strippedLocationHistory = locationHistory.trimUnOrdered();
                LocationHistory.UnorderedHistoryTrimer st = new LocationHistory.UnorderedHistoryTrimer();
                LocationHistory externalHistory = locationHistory.filter(st);

                logger.trace(String.format("Excluded %d", st.getExcluded().size()));

                if (!(strippedLocationHistory == externalHistory && externalHistory == null)) {
                    assertTrue(strippedLocationHistory.size() == externalHistory.size());
                    assertTrue(strippedLocationHistory.isOrdered(false));
                    assertTrue(externalHistory.isOrdered(false));
                    assertTrue(locationHistory.size() == (strippedLocationHistory.size()) + st.getExcluded().size());
                }

            }
        }
    }

    public static class TracksParams {

        int radius = 1000; /*meters*/
        boolean haltOnEnter;
        boolean exitTrack = false;

        public boolean isExitTrack() {
            return exitTrack;
        }

        public void setExitTrack(boolean exitTrack) {
            this.exitTrack = exitTrack;
        }

        public boolean isHaltOnEnter() {
            return haltOnEnter;
        }

        public void setHaltOnEnter(boolean haltOnEnter) {
            this.haltOnEnter = haltOnEnter;
        }

        public int getRadius() {
            return radius;
        }

        public void setRadius(int radius) {
            this.radius = radius;
        }

        public TracksParams(int radius) {
            this.radius = radius;
            this.haltOnEnter = false;
        }

        public TracksParams(int radius, boolean haltOnEnter) {
            this.radius = radius;
            this.haltOnEnter = haltOnEnter;
        }

        public boolean shouldInclude(LocationHistory.UntilReachHistory info) {
            return info.getHistory().size() > 1;
        }


    }

    public void init() throws Exception {

        TracksParams paramsDweling = new TracksParams(1000, false);
        this.locationsHistoryDwelling1000m = prepareForAllTracks(paramsDweling);

        TracksParams paramsEntering = new TracksParams(1000, true);
        locationsHistoryEntering1000m = prepareForAllTracks(paramsEntering);

        TracksParams paramsExiting = new TracksParams(1000, true);
        paramsExiting.setExitTrack(true);
        locationsHistoryExiting1000m = prepareForAllTracks(paramsExiting);
    }

    public static void  assertTrue(boolean condition) {
        if (!condition) throw new RuntimeException("Condition failed (assertTrue)");
    }

    public static void  assertEquals(Object expected, Object real) {
        if (!expected.equals(real)) throw new RuntimeException("Condition failed (assertEquals)");
    }
}