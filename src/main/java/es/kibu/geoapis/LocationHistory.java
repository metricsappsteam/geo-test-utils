/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis;

import es.kibu.geoapis.common.es.kibu.geoapis.common.gpx.Utils;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * A class for handling a history of locations, filtering, ordering
 *
 * @author lrodriguez2002cu on 22/02/2016.
 */

public class LocationHistory implements Collection<Location> {

    private Logger logger = LoggerFactory.getLogger(LocationHistory.class);

    private List<Location> locations = new ArrayList<>();

    private DateTimeFormatter dateTimeFormatter = ISODateTimeFormat.dateTime();


    //region Default implementation of collection
    @Override
    public String toString() {
        return "LocationHistory{" +
                "locations=" + locations +
                '}';
    }

    @Override
    public int size() {
        return locations.size();
    }

    @Override
    public boolean isEmpty() {
        return locations.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return locations.contains(o);
    }

    @Override
    public Iterator<Location> iterator() {
        return locations.iterator();
    }

    @Override
    public Object[] toArray() {
        return locations.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return locations.toArray(ts);
    }

    @Override
    public boolean add(Location location) {
        return locations.add(location);
    }

    /*public  add(Location location){
        locations.add(location);
    }
    */
    @Override
    public boolean remove(Object o) {
        return locations.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return locations.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends Location> collection) {
        return locations.addAll(collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return locations.retainAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return locations.retainAll(collection);
    }

    @Override
    public void clear() {
        locations.clear();
    }
    //endregion

   /* public static LocationHistory formLocationUpdatesParamList(List<LocationUpdateParams> history) {
        if (history == null) return null;

        LocationHistory historyResult = new LocationHistory();

        for (LocationUpdateParams locationUpdateParams : history) {
            Location location = Location.fromLocationParams(locationUpdateParams);
            historyResult.add(location);
        }
        return historyResult;
    }*/

    public List<Location.DataPoint> getAllDataPoints() {

        List<Location.DataPoint> dataPoints = new ArrayList<>();

        for (Location location : locations) {
            dataPoints.addAll(location.getDataPointsSorted());
        }

        return dataPoints;
    }


    public boolean isOrdered(boolean ignoreNoTime) {

        boolean ordered = true;
        DateTime lastTime = null;
        for (Location location : this) {
            for (Location.DataPoint dataPoint : location.getPoints()) {
                DateTime date = dataPoint.getDate();
                if (lastTime == null) {
                    lastTime = date;
                } else {
                    boolean hasTime = !(date.getHourOfDay() == 0 && date.getMinuteOfHour() == 0 && date.getSecondOfMinute() == 0);
                    ordered = ordered && ((lastTime.isBefore(date) || lastTime.isEqual(date)) || (ignoreNoTime && !hasTime));

                    if ((ignoreNoTime && !hasTime)) {
                        logger.debug(String.format("ignoring: %s in order check ", dateTimeFormatter.print(date)));
                    }

                    if (!ordered){
                        logger.debug("Is not ordered because lastTime {} newTime {}", lastTime, date);
                        return ordered;
                    }
                    lastTime = date;

                }
            }
        }

        return ordered;
    }

    public Location last() {
        if (locations.size() > 0) {
            return locations.get(locations.size() - 1);
        }
        return null;
    }

    public LocationHistory filter(IterationCondition condition) {
        LocationHistory history = new LocationHistory();
        for (Location location : this.locations) {
            if (condition.conditionApplies(history, location)) {
                history.add(location);
            }
        }
        if (condition.isAValidSequence(history)) {

            return history;
        } else return null;
    }


    public LocationHistory shuffle(){
        LocationHistory history = new LocationHistory();
        long seed = System.nanoTime();
        history.addAll(locations);
        //List<Location> tochuffle = new ArrayList<>(locations);
        Collections.shuffle(history.locations, new Random(seed));
        return history;
    }

    public LocationHistory trimUnOrdered() {
        UnorderedHistoryTrimer stripper = new UnorderedHistoryTrimer();
        return this.filter(stripper);
    }

    public UntilReachHistory filterUntilReachPoint(Location onceReached, double radius) {
        LocationHistory.ReachInfoExtractor condition = new LocationHistory.ReachInfoExtractor(onceReached, radius);
        LocationHistory locationHistory = this.filter(condition);
        if (locationHistory != null) {
            return new UntilReachHistory(locationHistory, condition);
        }
        return null;
    }

    public Interval getRange(){
        DateTime begin = null;
        DateTime end = null;

        for (Location.DataPoint dataPoint : getAllDataPoints()) {
            begin = Utils.updateMin(begin, dataPoint.getDate());
            end = Utils.updateMax(end, dataPoint.getDate());
        }
        if (begin!= null &&  end!= null) {
            return new Interval(begin, end);
        }
        else return null;
    }

    //region types for supporting filtering
    public interface Condition {
        boolean conditionApplies(LocationHistory history, Location location);
    }

    public interface IterationCondition extends Condition {
        boolean conditionApplies(LocationHistory history, Location location);

        boolean isAValidSequence(LocationHistory filteredHistory);
    }


    public static class IntervalCondition implements LocationHistory.IterationCondition {

        Interval interval;

        public IntervalCondition(Interval interval) {
            this.interval = interval;
        }

        @Override
        public boolean conditionApplies(LocationHistory history, Location location) {
            Location.DataPoint dataPoint = location.getPoints().get(0);
            DateTime date = dataPoint.getDate();
            return interval.getStart().isBefore(date) || interval.getStart().isEqual(date)
                    || interval.getEnd().isAfter(date) || interval.getEnd().isEqual(date);
        }

        @Override
        public boolean isAValidSequence(LocationHistory filteredHistory) {
            return true;
        }

    }

    public static class UntilDateTimeCondition extends BaseReachInfo implements LocationHistory.IterationCondition {

        DateTime untilTime;

        public UntilDateTimeCondition(DateTime untilTime) {
            this.untilTime = untilTime;
        }

        @Override
        public boolean conditionApplies(LocationHistory history, Location location) {
            Location.DataPoint dataPoint = location.getPoints().get(0);
            return dataPoint.getDate().isBefore(untilTime);
        }

        @Override
        public boolean isAValidSequence(LocationHistory filteredHistory) {
            return true;
        }

    }


    public static class UntilReachHistory {
        LocationHistory history;

        ReachInfo info;

        public UntilReachHistory(LocationHistory history, ReachInfo info) {
            this.history = history;
            this.info = info;
        }

        public ReachInfo getInfo() {
            return info;
        }

        public LocationHistory getHistory() {
            return history;
        }
    }

    public static class UnorderedHistoryTrimer implements LocationHistory.IterationCondition, ExcludedInfo<Location> {

        public DateTime lastDate;
        List<Location> excluded = new ArrayList<>();

        @Override
        public boolean conditionApplies(LocationHistory history, Location location) {

            boolean ordered = true;
            for (Location.DataPoint dataPoint : location.getPoints()) {
                DateTime date = dataPoint.getDate();
                ordered = ordered && ((lastDate == null) || date.isAfter(lastDate));
                if (ordered) {
                    lastDate = date;
                }
            }

            if (!ordered) {
                //accumulatePartialResult the ones wont be included
                excluded.add(location);
            }

            return ordered;
        }

        @Override
        public boolean isAValidSequence(LocationHistory filteredHistory) {
            return true;
        }

        @Override
        public Collection<Location> getExcluded() {
            return excluded;
        }
    }


    public static class HighSpeedHistoryTrimmer implements LocationHistory.IterationCondition, ExcludedInfo<Location> {

        //public DateTime lastDate;
        Location.DataPoint lastDataPoint;
        List<Location> excluded = new ArrayList<>();
        double speedThreshold;

        public HighSpeedHistoryTrimmer(double speedThreshold) {
            this.speedThreshold = speedThreshold;
        }

        @Override
        public boolean conditionApplies(LocationHistory history, Location location) {

            boolean isIn = true;
            for (Location.DataPoint dataPoint : location.getPoints()) {
                //DateTime date = dataPoint.getDate();
                isIn = isIn && lastDataPoint == null  || speedBetween(lastDataPoint, dataPoint)/* date.isAfter(lastDate)*/;
                if (isIn) {
                    lastDataPoint = dataPoint;
                }
            }

            if (!isIn) {
                //accumulatePartialResult the ones wont be included
                excluded.add(location);
            }

            return isIn;
        }

        private boolean speedBetween(Location.DataPoint lastDataPoint, Location.DataPoint dataPoint) {

            double latitude1 = lastDataPoint.getLocation().latitude;
            double longitude1 = lastDataPoint.getLocation().longitude;

            double latitude2 = dataPoint.getLocation().latitude;
            double longitude2 = dataPoint.getLocation().longitude;

            double distance = GeoUtils.harvesineDistance(latitude1, longitude1, latitude2, longitude2);

            //Period period = new Period(lastDataPoint.getDate(), dataPoint.getDate());

            double speed = (distance /1000)/(dataPoint.getDate().getMillis() -  lastDataPoint.getDate().getMillis())/(1000 * 60 * 60);

            return speed <= speedThreshold;
        }

        @Override
        public boolean isAValidSequence(LocationHistory filteredHistory) {
            return true;
        }

        @Override
        public Collection<Location> getExcluded() {
            return excluded;
        }
    }

    public static class ReachInfoExtractor extends BaseReachInfo implements LocationHistory.IterationCondition, ExcludedInfo<Location> {


        Logger logger = LoggerFactory.getLogger(ReachInfoExtractor.class);

        List<Location> excluded = new ArrayList<>();

        boolean pointReached = false;
        boolean wasReached = false;

        boolean haltOnEnter = false;

        public boolean isHaltOnEnter() {
            return haltOnEnter;
        }

        public void setHaltOnEnter(boolean haltOnEnter) {
            this.haltOnEnter = haltOnEnter;
        }

        public ReachInfoExtractor(Location onceReached, double radius) {
            super(radius, onceReached);
        }

        protected GeoUtils.LatLongRadius llr = new GeoUtils.LatLongRadius();

        @Override
        public boolean conditionApplies(LocationHistory history, Location location) {

            if (isHaltOnEnter() && pointReached) {
                return false;
            }

            Location.DataPoint dataPoint = location.lastPoint();
            Location.DataPoint poi = getReachedLocation().lastPoint();

            double latitude = dataPoint.getLocation().getLatitude();
            double longitude = dataPoint.getLocation().getLongitude();

            llr.setLatitude(poi.getLocation().getLatitude());
            llr.setLongitude(poi.getLocation().getLongitude());
            llr.setRadius(getRadius());

            pointReached = GeoUtils.insideRadiusFromPoint(latitude, longitude, llr);
            double distance = GeoUtils.harvesineDistance(latitude, longitude, llr.getLatitude(), llr.getLongitude());

            if (pointReached) {
                wasReached = pointReached;
            }

            //will allow to include the the reached as well
            boolean reachedAndPrevReached = pointReached && wasReached;
            boolean result = (!pointReached && !wasReached) || reachedAndPrevReached;

            if (reachedAndPrevReached) {
                updateReachInfo(location.lastPoint(), distance);
            }

            addExcluded(location, result);
            return result;
        }

       /* public void updateReachInfo(Location.DataPoint poi, double distance) {
            minDistance = Utils.updateMin(minDistance, distance);
            maxDistance = Utils.updateMax(maxDistance, distance);
            if (pointsUnderReach == 0) {
                beginReach = poi.getDate();
            }
            endReach = Utils.updateMax(endReach, poi.getDate());
            pointsUnderReach++;
            logger.trace(String.format("under reach %d (%f meters)", pointsUnderReach, distance));
        }*/

        public void addExcluded(Location location, boolean result) {
            if (!result) {
                excluded.add(location);
            }
        }

        @Override
        public Collection<Location> getExcluded() {
            return null;
        }

        @Override
        public boolean isAValidSequence(LocationHistory filteredHistory) {
            return true;
        }
    }

    public static class ExitInfoExtractor extends ReachInfoExtractor {


        public ExitInfoExtractor(Location target, double radius) {
            super(target, radius);
        }


        boolean neverReachedBefore = true;

        @Override
        public boolean isAValidSequence(LocationHistory filteredHistory) {
            return !pointReached && wasReached;
        }

        @Override
        public boolean conditionApplies(LocationHistory history, Location location) {

            if (isHaltOnExit() && !pointReached && wasReached) {
                return false;
            }

            Location.DataPoint dataPoint = location.lastPoint();
            double latitude = dataPoint.getLocation().getLatitude();
            double longitude = dataPoint.getLocation().getLongitude();

            Location.DataPoint poi = getReachedLocation().lastPoint();
            llr.setLatitude(poi.getLocation().getLatitude());
            llr.setLongitude(poi.getLocation().getLongitude());
            llr.setRadius(getRadius());

            pointReached = GeoUtils.insideRadiusFromPoint(latitude, longitude, llr);
            double distance = GeoUtils.harvesineDistance(latitude, longitude, llr.getLatitude(), llr.getLongitude());

            //will allow to include the the reached as well
            boolean isTheExitCondition = !pointReached && wasReached && !neverReachedBefore;
            boolean result = ((pointReached && wasReached) || (pointReached && neverReachedBefore))
                    || isTheExitCondition;
            //      boolean b = (pointReached && wasReached) || (!pointReached && wasReached);

            if (pointReached) {
                wasReached = pointReached;
                neverReachedBefore = false;
            }

            if (result) {
                updateReachInfo(dataPoint, distance, !isTheExitCondition);
            }

            addExcluded(location, result);

            return result;
        }

        public boolean isHaltOnExit() {
            return isHaltOnEnter();
        }


    }
    //endregion

}
