/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis;

import es.kibu.geoapis.common.es.kibu.geoapis.common.gpx.*;
import junit.framework.TestCase;
import org.joda.time.Interval;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import es.kibu.geoapis.HistoryDataProducer.TracksParams;

import java.util.List;

/**
 *
 * Created by lrodriguez2002cu on 10/03/2016.
 * @author lrodriguez2002cu
 */
public class BaseHistoryDataTest extends TestCase {

    static Logger logger = LoggerFactory.getLogger(BaseHistoryDataTest.class);

    protected final HistoryDataProducer historyDataProducer = new HistoryDataProducer();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        historyDataProducer.init();

    }

    public List<LocationHistory.UntilReachHistory> prepareForAllTracks(TracksParams params) throws Exception {

        return historyDataProducer.prepareForAllTracks(params);
    }

    public List<LocationHistory.UntilReachHistory> prepareTracks(Gpx gpx, TracksParams params) {

        return historyDataProducer.prepareTracks(gpx, params);
    }

    void printTestPreamble(String testName) {
        logger.debug("/*-------------------------------------------------------------------------------------*/");
        logger.debug("/*-------------------------------------------------------------------------------------*/");
        logger.debug(String.format("TEST: %s", testName.toUpperCase()));
    }

    boolean equalsReachInfo(ReachInfo info1, ReachInfo info2) {

        return historyDataProducer.equalsReachInfo(info1, info2);
    }

    protected void chekInfo(int radius, LocationHistory locationHistory, LocationHistory.UntilReachHistory info) {

        historyDataProducer.chekInfo(radius, locationHistory, info);
    }

    protected void checkTrackTime(Gpx gpx, TrkType trkType) {

        historyDataProducer.checkTrackTime(gpx, trkType);
    }

    public void testCheckHistoryFromTrack() throws Exception {
        Gpx gpx = SampleTracks.getGPX1().getGpx();
        LocationHistory locationHistory = historyDataProducer.historyFromTrack(gpx.getTrk().get(0), 3);
        assertTrue(locationHistory.size() == 3);
    }

    public void testCheckHistoryFromTrackWithTime() throws Exception {
        Gpx gpx = SampleTracks.getGPX1().getGpx();
        TrkType trkType = gpx.getTrk().get(0);
        historyDataProducer.checkTrackTime(gpx, trkType);
    }

    public void testCheckHistoryFromPoints() throws Exception {

        Gpx gpx = SampleTracks.getGPX1().getGpx();
        WptType wptType = gpx.getWpt().get(0);

        Location onceReached = historyDataProducer.fromGPXWayPoint(wptType);

        TrkType trkType = gpx.getTrk().get(0);

        TracksParams params = new TracksParams(1000);

        LocationHistory.UntilReachHistory info = historyDataProducer.historyFromTrackWithInfo(trkType, onceReached, params/*meters*/);
        if (info!= null) {
            LocationHistory locationHistory = info.getHistory();

            LocationHistory.UntilReachHistory info2 = locationHistory.filterUntilReachPoint(onceReached, params.radius);
            LocationHistory locationHistory2 = info2.getHistory();

            assertTrue(locationHistory.size() == locationHistory2.size());
            assertTrue(historyDataProducer.equalsReachInfo(info.getInfo(), info2.getInfo()));
            historyDataProducer.chekInfo(params.radius, locationHistory, info);
        }

    }

    public void testCheckHistoryFromPointsAll() throws Exception {
        SampleTracks.Sample sample = SampleTracks.getGPX1();
        TracksParams params = new TracksParams(1000);
        historyDataProducer.checkTrack(sample.getGpx(), sample.getSampleName(), params);
    }


    public void testCheckAllTracksHistoryFromPointsAll() throws Exception {
        TracksParams params = new TracksParams(1000);
        for (SampleTracks.Sample sample : SampleTracks.getAllTracks()) {
            Gpx gpx1 = sample.getGpx();
            historyDataProducer.checkTrack(gpx1, sample.getSampleName(), params);
        }
    }

    @Test
    public void testCheckAllTracksHistoryStripping() throws Exception {
        for (SampleTracks.Sample sample : SampleTracks.getAllTracks()) {
            Gpx gpx1 = sample.getGpx();
            historyDataProducer.checkStripper(gpx1);
        }
    }

    @Test
    public void testCheckAllTracksHistoryExiting() throws Exception {
        List<LocationHistory.UntilReachHistory> locationsHistoryExiting1000m = historyDataProducer.getLocationsHistoryExiting1000m();
        logger.debug("Testing with {} tracks", locationsHistoryExiting1000m.size());

        assertTrue(locationsHistoryExiting1000m.size()>0);

        for (LocationHistory.UntilReachHistory untilReachHistory : locationsHistoryExiting1000m) {
            assertTrue(untilReachHistory.getHistory().size()>0);
            ReachInfo info = untilReachHistory.getInfo();
            assertTrue(untilReachHistory.getHistory().size()> info.getPointsUnderReach());
            assertTrue((untilReachHistory.getHistory().size()- info.getPointsUnderReach()) == 1);
            Interval range = untilReachHistory.getHistory().getRange();
            assertTrue(Utils.isContained(range.getStart(), range.getEnd(), info.getBeginTime(), false));
            assertTrue(Utils.isContained(range.getStart(), range.getEnd(), info.getEndTime(), false));
        }

    }


    @Test
    public void testCheckTrackTeton() throws Exception {
        TracksParams params = new TracksParams(1000);

        for (SampleTracks.Sample sample : SampleTracks.getAllTracks()) {
            if (sample.getSampleName().contains("teton")) {
                Gpx gpx1 = sample.getGpx();
                historyDataProducer.checkTrack(gpx1, sample.getSampleName(), params);
            }
        }
    }
/*

    public interface ConditionsEvaluator {

        boolean evaluateConditions(RuleCheckParams ruleCheckParams, ReachInfo info, LocationHistory locationHistory,
                                   FencesRule fencesRule, RuleCheckResult ruleCheckResult);

    }

    public static class DefaultConditionsEvaluator implements ConditionsEvaluator {

        public DefaultConditionsEvaluator(boolean successOnAllInsideRadius) {
            this.successOnAllInsideRadius = successOnAllInsideRadius;
        }

        public DefaultConditionsEvaluator() {

        }

        boolean successOnAllInsideRadius = true;

        public boolean isSuccessOnAllInsideRadius() {
            return successOnAllInsideRadius;
        }

        public void setSuccessOnAllInsideRadius(boolean successOnAllInsideRadius) {
            this.successOnAllInsideRadius = successOnAllInsideRadius;
        }

        public boolean evaluateConditions(RuleCheckParams ruleCheckParams, ReachInfo info, LocationHistory locationHistory, FencesRule fencesRule, RuleCheckResult ruleCheckResult) {
            //the rule applies only if a change occurs
            boolean allWereInsideTheRadius = allWereInsideReachInfoRadius(info, locationHistory);

            boolean someWhereInsideSomeOutsideTheRadius = info.getPointsUnderReach() < locationHistory.size();

            boolean ruleApplies = whateverIsExpectedAndRuleApplies(ruleCheckParams, fencesRule, ruleCheckResult);
            //if none then the rules applies if thats exactly the result
            ruleApplies = haveNoneIsGoodIfThatsExpected(ruleCheckParams, fencesRule, ruleCheckResult, ruleApplies);

            boolean meetsPattern = ruleApplies || (ruleCheckResult.getRulesAndResults().get(fencesRule).wasTriggered()
                    && checkSpatialPattern(fencesRule, ruleCheckResult.getRulesAndResults()
                    .get(fencesRule).getResultData(), ruleCheckParams.getCheckFor()));

            if (ruleCheckParams.getCheckFor() == SpatialBehaviorPatternAnalyzer.SpatialBehaviorPattern.NONE) {
                //if none is specified, then check that thats the result.
                meetsPattern = checkSpatialPattern(fencesRule, ruleCheckResult.getRulesAndResults()
                        .get(fencesRule).getResultData(), ruleCheckParams.getCheckFor());
            }

            */
/*The fact that all are inside the radius makes the *//*

            return (successOnAllInsideRadius && allWereInsideTheRadius) || (someWhereInsideSomeOutsideTheRadius && meetsPattern);
        }

        public boolean haveNoneIsGoodIfThatsExpected(RuleCheckParams ruleCheckParams, FencesRule fencesRule, RuleCheckResult ruleCheckResult, boolean ruleApplies) {
            return ruleCheckParams.getCheckFor() == SpatialBehaviorPatternAnalyzer.SpatialBehaviorPattern.NONE ?
                    !ruleCheckResult.getRulesAndResults().get(fencesRule).wasTriggered() : ruleApplies;
        }

        public boolean whateverIsExpectedAndRuleApplies(RuleCheckParams ruleCheckParams, FencesRule fencesRule, RuleCheckResult ruleCheckResult) {
            return (ruleCheckParams.getCheckFor() == null)
                    && ruleCheckResult.getRulesAndResults().get(fencesRule).wasTriggered();
        }

        public boolean allWereInsideReachInfoRadius(ReachInfo info, LocationHistory locationHistory) {
            return info.getPointsUnderReach() == locationHistory.size();
        }

    }

    public static boolean checkSpatialPattern(FencesRule fencesRule, Object resultData, SpatialBehaviorPatternAnalyzer.SpatialBehaviorPattern checkFor) {
        if (fencesRule instanceof BaseFencesRule.SpatialTimedRule) {
            List resultDataList = (List) resultData;
            assertTrue(resultDataList.size() > 0);
            for (Object o : resultDataList) {
                boolean meetsPattern = ((BaseFencesRule.TrajectoryBehaviourRule.BehaviourResult) o).getPattern() == checkFor;
                assertTrue(meetsPattern);
                logger.debug(String.format("Checked the pattern condition with result: %s", Boolean.toString(meetsPattern)));
            }
        }
        return true;
    }
*/

}
