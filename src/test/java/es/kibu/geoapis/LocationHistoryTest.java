/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis;

import es.kibu.geoapis.common.es.kibu.geoapis.common.gpx.GPXApi;
import es.kibu.geoapis.common.es.kibu.geoapis.common.gpx.Gpx;
import es.kibu.geoapis.common.es.kibu.geoapis.common.gpx.SampleTracks;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by lrodr_000 on 18/05/2016.
 */
public class LocationHistoryTest {

    Logger logger = LoggerFactory.getLogger(LocationHistoryTest.class);

    @Test
    public void shuffle() throws Exception {

        HistoryDataProducer producer = new HistoryDataProducer();
        producer.init();

        List<LocationHistory.UntilReachHistory> locationsHistoryEntering1000m = producer.getLocationsHistoryEntering1000m();

        if (locationsHistoryEntering1000m.size() > 0) {
            //preparation phase
            LocationHistory.UntilReachHistory untilReachHistory = locationsHistoryEntering1000m.get(0);
            LocationHistory history = untilReachHistory.getHistory();

            LocationHistory shuffledHistory = history.shuffle();
            boolean ordered = shuffledHistory.isOrdered(true);
            assertFalse(ordered);

        }

    }


    @Test
    public void speedFilter() throws Exception {

        HistoryDataProducer producer = new HistoryDataProducer();
        producer.init();

        SampleTracks.Sample sample = SampleTracks.getGPX("albertotrack.xml");
        Gpx gpx = (Gpx) sample.getGpx();

        LocationHistory history = producer.historyFromTrack(gpx.getTrk().get(0));
        LocationHistory filter = history.filter(new LocationHistory.HighSpeedHistoryTrimmer(47));

        logger.debug("elems: {}", filter.getAllDataPoints().size());

/*
        if (locationsHistoryEntering1000m.size() > 0) {
            //preparation phase
            LocationHistory.UntilReachHistory untilReachHistory = locationsHistoryEntering1000m.get(0);
            LocationHistory history = untilReachHistory.getHistory();

            LocationHistory shuffledHistory = history.shuffle();
            boolean ordered = shuffledHistory.isOrdered(true);
            assertFalse(ordered);

        }
*/

    }

   /* @Test
    public void sort() throws Exception {

        HistoryDataProducer producer = new HistoryDataProducer();
        producer.init();

        List<LocationHistory.UntilReachHistory> locationsHistoryEntering1000m = producer.getLocationsHistoryEntering1000m();

        if (locationsHistoryEntering1000m.size()>0) {
            //preparation phase
            LocationHistory.UntilReachHistory untilReachHistory = locationsHistoryEntering1000m.get(0);
            LocationHistory history = untilReachHistory.getHistory();

            LocationHistory shuffledHistory = history.shuffle();
            boolean ordered = shuffledHistory.isOrdered(true);

            Assert.assertFalse(ordered);

        }

    }*/
}